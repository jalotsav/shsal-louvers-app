package com.jalotsav.shsallouvers;

import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.NavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

import com.jalotsav.shsallouvers.bottomnvgtnmain.FrgmntAboutUs;
import com.jalotsav.shsallouvers.bottomnvgtnmain.FrgmntContactUs;
import com.jalotsav.shsallouvers.bottomnvgtnmain.FrgmntProfile;
import com.jalotsav.shsallouvers.bottomnvgtnmain.FrgmntProjects;
import com.jalotsav.shsallouvers.bottomnvgtnmain.FrgmntQuotation;
import com.jalotsav.shsallouvers.bottomnvgtnmain.FrgmntVideos;
import com.jalotsav.shsallouvers.common.AppConstants;
import com.jalotsav.shsallouvers.common.UserSessionManager;

import butterknife.BindString;
import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Jalotsav on 5/26/18.
 */

public class ActvtyMain extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener {

    @BindView(R.id.toolbar_drwrlyot_appbar_main) Toolbar mToolbar;
    @BindView(R.id.drwrlyot_nvgtndrwr_main) DrawerLayout mDrwrlyot;
    @BindView(R.id.navgtnvw_nvgtndrwr_main) NavigationView mNavgtnVw;

    @BindString(R.string.logout_sml) String mStrLogout;
    @BindString(R.string.logout_alrtdlg_msg) String mStrLogoutMsg;

    TextView mTvFullName, mTvEmailMobile;

    MenuItem mMenuItemProjects, mMenuItemVideos, mMenuItemQuotation, mMenuItemAboutUs, mMenuItemContactUs, mMenuItemProfile;
    UserSessionManager session;
    Bundle mBundle;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.lo_actvty_main);
        ButterKnife.bind(this);

        setSupportActionBar(mToolbar);

        session = new UserSessionManager(this);

        if(session.checkLogin()) {

            ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                    this, mDrwrlyot, mToolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
            mDrwrlyot.addDrawerListener(toggle);
            toggle.syncState();

            mNavgtnVw.setNavigationItemSelectedListener(this);

            View mVwHeaderlyot = mNavgtnVw.getHeaderView(0);
            mTvFullName = mVwHeaderlyot.findViewById(R.id.tv_drwrlyot_header_fullname);
            mTvEmailMobile = mVwHeaderlyot.findViewById(R.id.tv_drwrlyot_header_emailmobile);
            mTvEmailMobile.setText(session.getMobile());

            mTvFullName.setText(session.getFirstName().concat(" ").concat(session.getLastName()));

            // load Projects fragment by default
            int navgtnPosition = getIntent().getIntExtra(AppConstants.PUT_EXTRA_NVGTNVW_POSTN, AppConstants.NVGTNVW_PROJECTS);
            onNavigationItemSelected(getNavgtnvwPositionMenuItem(navgtnPosition));
        }
    }

    // Get MenuItem from BottomNavigationView Position, which is get from getIntent()
    private MenuItem getNavgtnvwPositionMenuItem(int navgtnPosition) {

        switch (navgtnPosition) {
            case AppConstants.NVGTNVW_PROJECTS:

                return mNavgtnVw.getMenu().findItem(R.id.action_nvgtndrwr_main_projects);
            case AppConstants.NVGTNVW_VIDEOS:

                return mNavgtnVw.getMenu().findItem(R.id.action_nvgtndrwr_main_videos);
            case AppConstants.NVGTNVW_QUOTATION:

                return mNavgtnVw.getMenu().findItem(R.id.action_nvgtndrwr_main_quotation);
            case AppConstants.NVGTNVW_ABOUTUS:

                return mNavgtnVw.getMenu().findItem(R.id.action_nvgtndrwr_main_aboutus);
            case AppConstants.NVGTNVW_CONTACTUS:

                return mNavgtnVw.getMenu().findItem(R.id.action_nvgtndrwr_main_contactus);
            case AppConstants.NVGTNVW_PROFIE:

                return mNavgtnVw.getMenu().findItem(R.id.action_nvgtndrwr_main_profile);
            default:

                return mNavgtnVw.getMenu().findItem(R.id.action_nvgtndrwr_main_projects);
        }
    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {

        Fragment fragment;
        if(mBundle == null) {

            mBundle = new Bundle();
            mBundle.putInt(AppConstants.PUT_EXTRA_COME_FROM, 0);
        }

        switch (item.getItemId()) {
            case R.id.action_nvgtndrwr_main_projects:

                fragment = new FrgmntProjects();
                mToolbar.setTitle(getString(R.string.projects_sml));
                loadFragment(fragment, item);
                return true;
            case R.id.action_nvgtndrwr_main_videos:

                fragment = new FrgmntVideos();
                mToolbar.setTitle(getString(R.string.videos_sml));
                loadFragment(fragment, item);
                return true;
            case R.id.action_nvgtndrwr_main_quotation:

                fragment = new FrgmntQuotation();
                mToolbar.setTitle(getString(R.string.quotation_sml));
                loadFragment(fragment, item);
                return true;
            case R.id.action_nvgtndrwr_main_aboutus:

                fragment = new FrgmntAboutUs();
                mToolbar.setTitle(getString(R.string.aboutus_sml));
                loadFragment(fragment, item);
                return true;
            case R.id.action_nvgtndrwr_main_contactus:

                fragment = new FrgmntContactUs();
                mToolbar.setTitle(getString(R.string.contactus_sml));
                loadFragment(fragment, item);
                return true;
            case R.id.action_nvgtndrwr_main_profile:

                fragment = new FrgmntProfile();
                mToolbar.setTitle(getString(R.string.profile_sml));
                loadFragment(fragment, item);
                return true;
            case R.id.action_nvgtndrwr_main_logout:

                confirmLogoutAlertDialog();
                return false;
        }
        return false;
    }

    // Load Fragment of selected MenuItem
    private void loadFragment(Fragment fragment, MenuItem item) {

        if(fragment != null) {

            mDrwrlyot.closeDrawer(GravityCompat.START);

            getCurrentCheckedMenuItem().setChecked(false);
            item.setChecked(true);

            fragment.setArguments(mBundle);
            FragmentManager fragmentManager = getSupportFragmentManager();
            fragmentManager
                    .beginTransaction()
                    .replace(R.id.framlyot_drwrlyot_contnt_container, fragment)
                    .commit();
        }
    }

    // Get Current checked MenuItem of NavigationView
    public MenuItem getCurrentCheckedMenuItem() {

        mMenuItemProjects = mNavgtnVw.getMenu().findItem(R.id.action_nvgtndrwr_main_projects);
        mMenuItemVideos = mNavgtnVw.getMenu().findItem(R.id.action_nvgtndrwr_main_videos);
        mMenuItemQuotation = mNavgtnVw.getMenu().findItem(R.id.action_nvgtndrwr_main_quotation);
        mMenuItemAboutUs = mNavgtnVw.getMenu().findItem(R.id.action_nvgtndrwr_main_aboutus);
        mMenuItemContactUs = mNavgtnVw.getMenu().findItem(R.id.action_nvgtndrwr_main_contactus);
        mMenuItemProfile = mNavgtnVw.getMenu().findItem(R.id.action_nvgtndrwr_main_profile);

        MenuItem currntSelctdMenuItem;
        if (mMenuItemProjects.isChecked())
            currntSelctdMenuItem = mMenuItemProjects;
        else if (mMenuItemVideos.isChecked())
            currntSelctdMenuItem = mMenuItemVideos;
        else if (mMenuItemQuotation.isChecked())
            currntSelctdMenuItem = mMenuItemQuotation;
        else if (mMenuItemAboutUs.isChecked())
            currntSelctdMenuItem = mMenuItemAboutUs;
        else if (mMenuItemContactUs.isChecked())
            currntSelctdMenuItem = mMenuItemContactUs;
        else if (mMenuItemProfile.isChecked())
            currntSelctdMenuItem = mMenuItemProfile;
        else
            currntSelctdMenuItem = mMenuItemProjects;

        return currntSelctdMenuItem;
    }

    // Show AlertDialog for confirm to Logout
    private void confirmLogoutAlertDialog() {

        mDrwrlyot.closeDrawer(GravityCompat.START);

        AlertDialog.Builder alrtDlg = new AlertDialog.Builder(this);
        alrtDlg.setTitle(mStrLogout);
        alrtDlg.setMessage(mStrLogoutMsg);
        alrtDlg.setNegativeButton(getString(R.string.no_sml).toUpperCase(), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                dialogInterface.dismiss();
            }
        });
        alrtDlg.setPositiveButton(getString(R.string.logout_sml).toUpperCase(), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {

                dialogInterface.dismiss();
                session.logoutUser();
            }
        });

        alrtDlg.show();
    }

    @Override
    public void onBackPressed() {

        if (mDrwrlyot.isDrawerOpen(GravityCompat.START))
            mDrwrlyot.closeDrawer(GravityCompat.START);
        else if (getCurrentCheckedMenuItem() != mMenuItemProjects)
            onNavigationItemSelected(mNavgtnVw.getMenu().getItem(0));
        else
            super.onBackPressed();
    }
}
