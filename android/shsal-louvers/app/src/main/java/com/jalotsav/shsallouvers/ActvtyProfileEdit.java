/*
 * Copyright (c) 2018 Jalotsav
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.jalotsav.shsallouvers;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.Snackbar;
import android.support.design.widget.TextInputEditText;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AppCompatActivity;
import android.text.Editable;
import android.text.Html;
import android.text.TextWatcher;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AutoCompleteTextView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.location.places.AutocompleteFilter;
import com.google.android.gms.location.places.Place;
import com.google.android.gms.location.places.PlaceBuffer;
import com.google.android.gms.location.places.Places;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.jalotsav.shsallouvers.adapter.PlaceArrayAdapter;
import com.jalotsav.shsallouvers.common.AppConstants;
import com.jalotsav.shsallouvers.common.GeneralFunctions;
import com.jalotsav.shsallouvers.common.UserSessionManager;
import com.jalotsav.shsallouvers.models.MdlUsers;
import com.jalotsav.shsallouvers.utils.ValidationUtils;

import butterknife.BindString;
import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Jalotsav on 1/1/2019.
 */
public class ActvtyProfileEdit extends AppCompatActivity implements AppConstants, GoogleApiClient.OnConnectionFailedListener,
        GoogleApiClient.ConnectionCallbacks {

    private static final String TAG = ActvtyProfileEdit.class.getSimpleName();

    @BindView(R.id.cordntrlyot_actvty_profile_edit) CoordinatorLayout mCrdntrlyot;

    @BindView(R.id.tv_actvty_profile_edit_firstcharname) TextView mTvFirstCharName;

    @BindView(R.id.txtinputlyot_actvty_profile_edit_firstname) TextInputLayout mTxtinptlyotFirstName;
    @BindView(R.id.txtinputlyot_actvty_profile_edit_lastname) TextInputLayout mTxtinptlyotLastName;
    @BindView(R.id.txtinputlyot_actvty_profile_edit_email) TextInputLayout mTxtinptlyotEmail;
    @BindView(R.id.txtinputlyot_actvty_profile_edit_companyname) TextInputLayout mTxtinptlyotCompanyName;
    @BindView(R.id.txtinputlyot_actvty_profile_edit_city) TextInputLayout mTxtinptlyotCity;

    @BindView(R.id.txtinptet_actvty_profile_edit_firstname) TextInputEditText mTxtinptEtFirstName;
    @BindView(R.id.txtinptet_actvty_profile_edit_lastname) TextInputEditText mTxtinptEtLastName;
    @BindView(R.id.txtinptet_actvty_profile_edit_email) TextInputEditText mTxtinptEtEmail;
    @BindView(R.id.txtinptet_actvty_profile_edit_companyname) TextInputEditText mTxtinptEtCompanyName;
    @BindView(R.id.autocmplttv_actvty_profile_edit_city) AutoCompleteTextView mAutocmplttvCity;

    @BindView(R.id.prgrsbr_actvty_profile_edit) ProgressBar mPrgrsbrMain;

    @BindString(R.string.no_intrnt_cnctn) String mNoInternetConnMsg;
    @BindString(R.string.server_problem_sml) String mServerPrblmMsg;
    @BindString(R.string.entr_firstname_sml) String mEntrFirstName;
    @BindString(R.string.entr_lastname_sml) String mEntrLastName;
    @BindString(R.string.entr_email_sml) String mEntrEmail;
    @BindString(R.string.email_exist) String mEmailExistMsg;
    @BindString(R.string.entr_valid_city_sml) String mEntrSelectValidCity;

    private static final int GOOGLE_API_CLIENT_ID = 0;
    private GoogleApiClient mGoogleApiClient;
    private PlaceArrayAdapter mPlaceArrayAdapter;
    String mSelectedCityVal, mFirstNameVal, mLastNameVal, mEmailVal, mCompanyNameVal = "";
    UserSessionManager session;
    FirebaseDatabase mDatabase;
    DatabaseReference mRootRef, mUsersRef;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.lo_actvty_profile_edit);
        ButterKnife.bind(this);

        try {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_action_close);
        } catch (Exception e) {
            e.printStackTrace();
        }

        session = new UserSessionManager(this);

        fetchUserDetails();
//
        // Firebase Database Initialization and References
        mDatabase = FirebaseDatabase.getInstance();
        mRootRef = mDatabase.getReference().child(ROOT_NAME);
        mUsersRef = mRootRef.child(CHILD_USERS);

        // Google Place API configuration
        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .addApi(Places.GEO_DATA_API)
                .enableAutoManage(this, GOOGLE_API_CLIENT_ID, this)
                .addConnectionCallbacks(this)
                .build();

        mAutocmplttvCity.setThreshold(3);
        mAutocmplttvCity.setOnItemClickListener(mAutocompleteClickListener);
        AutocompleteFilter autocompleteFilter = new AutocompleteFilter.Builder()
                .setTypeFilter(AutocompleteFilter.TYPE_FILTER_CITIES)
                /*.setCountry("IN")*/
                .build();
        mPlaceArrayAdapter = new PlaceArrayAdapter(this, android.R.layout.simple_list_item_1, null, autocompleteFilter);
        mAutocmplttvCity.setAdapter(mPlaceArrayAdapter);

        mTxtinptEtFirstName.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int start, int before, int count) {

                // Condition TRUE when only 1st character changed.
                if(start == 0 && before == 1 && count == 0 || start == 0 && before == 0 && count == 1) {
                    if(charSequence.toString().length() != 0)
                        mTvFirstCharName.setText(charSequence.toString().substring(0, 1).toUpperCase());
                    else
                        mTvFirstCharName.setText("");
                }
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });
    }

    // Fetch User Profile Details from SharedPreferences
    private void fetchUserDetails() {

        mTvFirstCharName.setText(session.getFirstName().substring(0,1).toUpperCase());
        mTxtinptEtFirstName.setText(session.getFirstName());
        mTxtinptEtLastName.setText(session.getLastName());
        mTxtinptEtEmail.setText(session.getEmail());
        mSelectedCityVal = session.getCity();
        mAutocmplttvCity.setText(mSelectedCityVal);
        mTxtinptEtCompanyName.setText(session.getCompanyName());
    }

    // ItemClickListener for AutoComplete of City
    private AdapterView.OnItemClickListener mAutocompleteClickListener = new AdapterView.OnItemClickListener() {

        @Override
        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

            final PlaceArrayAdapter.PlaceAutocomplete item = mPlaceArrayAdapter.getItem(position);
            final String placeId = String.valueOf(item.placeId);
            Log.i(TAG, "Selected: " + item.description);
            PendingResult<PlaceBuffer> placeResult = Places.GeoDataApi.getPlaceById(mGoogleApiClient, placeId);
            placeResult.setResultCallback(mUpdatePlaceDetailsCallback);
            Log.i(TAG, "Fetching details for ID: " + item.placeId);
        }
    };

    private ResultCallback<PlaceBuffer> mUpdatePlaceDetailsCallback = new ResultCallback<PlaceBuffer>() {

        @Override
        public void onResult(PlaceBuffer places) {

            if (!places.getStatus().isSuccess()) {
                Log.e(TAG, "Place query did not complete. Error: " + places.getStatus().toString());
                return;
            }
            // Selecting the first object buffer.
            final Place place = places.get(0);
            mSelectedCityVal = String.valueOf(Html.fromHtml(place.getAddress() + ""));
        }
    };

    // Check all validation of fields and call API
    private void checkAllValidation() {

        if (!ValidationUtils.validateEmpty(this, mTxtinptlyotFirstName, mTxtinptEtFirstName, mEntrFirstName)) // FirstName
            return;

        if (!ValidationUtils.validateEmpty(this, mTxtinptlyotLastName, mTxtinptEtLastName, mEntrLastName)) // LastName
            return;

        if (!ValidationUtils.validateEmpty(this, mTxtinptlyotEmail, mTxtinptEtEmail, mEntrEmail)) // Email
            return;

        if (!ValidationUtils.validateEmailFormat(this, mTxtinptlyotEmail, mTxtinptEtEmail)) // Email Format
            return;

        if (!ValidationUtils.validateEmpty(mTxtinptlyotCity, mSelectedCityVal, mEntrSelectValidCity)) // City
            return;
        mAutocmplttvCity.setText(mSelectedCityVal); // Set selected value from Place API, in case user changed after selection

        if(!session.getEmail().equalsIgnoreCase(mTxtinptEtEmail.getText().toString().trim()))
            checkEmailExist();
        else
            updateUser();
    }

    // Check for Email is already exist in our Firebase Database
    private void checkEmailExist() {

        mPrgrsbrMain.setVisibility(View.VISIBLE);
        mEmailVal = mTxtinptEtEmail.getText().toString().trim();
        // Check Email is Exist in database
        mUsersRef.orderByChild(CHILD_EMAIL).equalTo(mEmailVal)
                .addListenerForSingleValueEvent(new ValueEventListener() {
                    @Override
                    public void onDataChange(@NonNull DataSnapshot dataSnapshot) {

                        mPrgrsbrMain.setVisibility(View.GONE);
                        if(dataSnapshot.exists())
                            Snackbar.make(mCrdntrlyot, mEmailExistMsg, Snackbar.LENGTH_LONG).show();
                        else
                            updateUser();
                    }

                    @Override
                    public void onCancelled(@NonNull DatabaseError databaseError) {

                        Log.e(TAG, "checkEmailExist - onCancelled: " + databaseError.getMessage());
                        mPrgrsbrMain.setVisibility(View.GONE);
                        Snackbar.make(mCrdntrlyot, mServerPrblmMsg, Snackbar.LENGTH_LONG).show();
                    }
                });
    }

    // Update user in Firebase Database
    private void updateUser() {

        mFirstNameVal =  mTxtinptEtFirstName.getText().toString().trim();
        mFirstNameVal = Character.toUpperCase(mFirstNameVal.charAt(0))+mFirstNameVal.substring(1); // First Character Uppercase
        mLastNameVal = mTxtinptEtLastName.getText().toString().trim();
        mLastNameVal = Character.toUpperCase(mLastNameVal.charAt(0)) + mLastNameVal.substring(1); // First Character Uppercase
        mEmailVal = mTxtinptEtEmail.getText().toString().trim();
        mCompanyNameVal = mTxtinptEtCompanyName.getText().toString().trim();

        MdlUsers updatedMdlUsers = new MdlUsers(mFirstNameVal, mLastNameVal, mEmailVal, mCompanyNameVal, mSelectedCityVal);
        DatabaseReference currentUserRef = mUsersRef.child(session.getUserId());
        currentUserRef.updateChildren(
                updatedMdlUsers.toMap())
                .addOnSuccessListener(new OnSuccessListener<Void>() {
                    @Override
                    public void onSuccess(Void aVoid) {

                        Log.i(TAG, "onSuccess: updateUser - data successfully updated.");
                        session.setFirstName(mFirstNameVal);
                        session.setLastName(mLastNameVal);
                        session.setEmail(mEmailVal);
                        session.setCompanyName(mCompanyNameVal);
                        session.setCity(mSelectedCityVal);

                        Toast.makeText(ActvtyProfileEdit.this, getString(R.string.profile_saved_msg, mFirstNameVal.concat(" ").concat(mLastNameVal)), Toast.LENGTH_SHORT).show();
                        setResult(RESULT_OK);
                        finish();
                    }
                })
                .addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {

                        Log.e(TAG, "onFailure: updateUser - " + e.getMessage());
                        Snackbar.make(mCrdntrlyot, mServerPrblmMsg, Snackbar.LENGTH_LONG).show();
                    }
                });
    }

    @Override
    public void onConnected(Bundle bundle) {

        mPlaceArrayAdapter.setGoogleApiClient(mGoogleApiClient);
        Log.i(TAG, "Google Places API connected.");
    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

        Log.e(TAG, "Google Places API connection failed with error code: " + connectionResult.getErrorCode());
        Toast.makeText(this, mServerPrblmMsg, Toast.LENGTH_LONG).show();
    }

    @Override
    public void onConnectionSuspended(int i) {
        mPlaceArrayAdapter.setGoogleApiClient(null);
        Log.e(TAG, "Google Places API connection suspended.");
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_done, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()){
            case android.R.id.home:
                onBackPressed();
                break;
            case R.id.action_done:

                if(mPrgrsbrMain.getVisibility() != View.VISIBLE) {
                    if (GeneralFunctions.isNetConnected(this))
                        checkAllValidation();
                    else
                        Snackbar.make(mCrdntrlyot, mNoInternetConnMsg, Snackbar.LENGTH_LONG).show();
                }
                break;
        }
        return super.onOptionsItemSelected(item);
    }
}
