/*
 * Copyright (c) 2018 Jalotsav
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.jalotsav.shsallouvers;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.Snackbar;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.firebase.ui.database.FirebaseRecyclerAdapter;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.jalotsav.shsallouvers.common.AppConstants;
import com.jalotsav.shsallouvers.common.GeneralFunctions;
import com.jalotsav.shsallouvers.common.RecyclerViewEmptySupport;
import com.jalotsav.shsallouvers.models.MdlImages;
import com.jalotsav.shsallouvers.viewholders.ImagesViewHolder;

import java.util.ArrayList;

import butterknife.BindString;
import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Jalotsav on 5/30/18.
 */

public class ActvtyProjectsImages extends AppCompatActivity implements AppConstants {

    @BindView(R.id.cordntrlyot_actvty_projects_images) CoordinatorLayout mCrdntrlyot;
    @BindView(R.id.lnrlyot_recyclremptyvw_appearhere) LinearLayout mLnrlyotAppearHere;
    @BindView(R.id.tv_actvty_projects_images_title) TextView mTvTitle;
    @BindView(R.id.tv_recyclremptyvw_appearhere) TextView mTvAppearHere;
    @BindView(R.id.rcyclrvw_actvty_projects_images) RecyclerViewEmptySupport mRecyclerView;
    @BindView(R.id.prgrsbr_actvty_projects_images) ProgressBar mPrgrsbr;

    @BindString(R.string.no_intrnt_cnctn) String mNoInternetConnMsg;
    @BindString(R.string.projects_appear_here) String mProjectsAppearHere;

    RecyclerView.LayoutManager mLayoutManager;
    ArrayList<MdlImages> mArrylstMdlImages;
    FirebaseRecyclerAdapter<MdlImages, ImagesViewHolder> mAdapter;
    FirebaseDatabase mDatabase;
    DatabaseReference mRootRef, mProjectsRef, mImagesRef;
    String mSlctdProjectChildKey, mSlctdProjectName;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.lo_actvty_projects_images);
        ButterKnife.bind(this);

        try {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setElevation(0);
            setTitle("");
        } catch (Exception e) {
            e.printStackTrace();
        }

        mSlctdProjectChildKey = getIntent().getStringExtra(PUT_EXTRA_DBREF_CHILD_KEY);
        mSlctdProjectName = getIntent().getStringExtra(PUT_EXTRA_PROJECT_NAME);

        mTvTitle.setText(mSlctdProjectName);

        mLayoutManager = new GridLayoutManager(this, 2);
        mRecyclerView.setHasFixedSize(true);
        mRecyclerView.setLayoutManager(mLayoutManager);
        mRecyclerView.setEmptyView(mLnrlyotAppearHere);

        mTvAppearHere.setText(mProjectsAppearHere);

        // Firebase Database Initialization and References
        mDatabase = FirebaseDatabase.getInstance();
        mRootRef = mDatabase.getReference().child(ROOT_NAME);
        mProjectsRef = mRootRef.child(CHILD_PROJECTS);
        mImagesRef = mProjectsRef.getRef().child(mSlctdProjectChildKey).child(CHILD_IMAGES);

        if(GeneralFunctions.isNetConnected(this)) {

            setupFirebaseAdapter();
            getProjectAllImages();
        } else Snackbar.make(mCrdntrlyot, mNoInternetConnMsg, Snackbar.LENGTH_LONG).show();
    }

    // Setup Recycler Adapter using Firebase-UI with Realtime database support.
    private void setupFirebaseAdapter() {

        mPrgrsbr.setVisibility(View.VISIBLE);
        mAdapter = new FirebaseRecyclerAdapter<MdlImages, ImagesViewHolder>(
                MdlImages.class,
                R.layout.lo_recyclritem_images,
                ImagesViewHolder.class,
                mImagesRef
        ) {
            @Override
            public void onDataChanged() {
                super.onDataChanged();

                if(mPrgrsbr != null && mPrgrsbr.getVisibility() == View.VISIBLE)
                    mPrgrsbr.setVisibility(View.GONE);
                mAdapter.notifyDataSetChanged();
            }

            @Override
            protected void populateViewHolder(ImagesViewHolder viewHolder, MdlImages model, final int position) {

                viewHolder.setImage(ActvtyProjectsImages.this, model.getImageURL());

                viewHolder.itemView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {

                        Bundle bundle = new Bundle();
                        bundle.putSerializable(PUT_EXTRA_IMAGES_MODEL, mArrylstMdlImages);
                        bundle.putInt(PUT_EXTRA_POSITION, position);

                        FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
                        DialogFrgmntImagePreview frgmntImagePreview = DialogFrgmntImagePreview.newInstance();
                        frgmntImagePreview.setArguments(bundle);
                        frgmntImagePreview.show(ft, "imagePreview");
                    }
                });
            }
        };
        mRecyclerView.setAdapter(mAdapter);
    }

    /*
    * Get All IMAGES of selected project from Firebase
    * and store into ArrayList, use for Preview Image screen
    * */
    private void getProjectAllImages() {

        mArrylstMdlImages = new ArrayList<>();

        mImagesRef.addChildEventListener(new ChildEventListener() {
            @Override
            public void onChildAdded(@NonNull DataSnapshot dataSnapshot, @Nullable String previousChildName) {

                mArrylstMdlImages.add(dataSnapshot.getValue(MdlImages.class));
            }

            @Override
            public void onChildChanged(@NonNull DataSnapshot dataSnapshot, @Nullable String previousChildName) {

            }

            @Override
            public void onChildRemoved(@NonNull DataSnapshot dataSnapshot) {

                try {
                    mArrylstMdlImages.remove(dataSnapshot.getValue(MdlImages.class));
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onChildMoved(@NonNull DataSnapshot dataSnapshot, @Nullable String previousChildName) {

            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_refresh, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()){
            case android.R.id.home:
                onBackPressed();
                break;
            case R.id.action_refresh:

                if(GeneralFunctions.isNetConnected(this)) {

                    setupFirebaseAdapter();
                    getProjectAllImages();
                } else Snackbar.make(mCrdntrlyot, mNoInternetConnMsg, Snackbar.LENGTH_LONG).show();
                break;
        }
        return super.onOptionsItemSelected(item);
    }
}
