/*
 * Copyright (c) 2018 Jalotsav
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.jalotsav.shsallouvers;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.Snackbar;
import android.support.design.widget.TextInputEditText;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.text.Html;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AutoCompleteTextView;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.location.places.AutocompleteFilter;
import com.google.android.gms.location.places.Place;
import com.google.android.gms.location.places.PlaceBuffer;
import com.google.android.gms.location.places.Places;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.jalotsav.shsallouvers.adapter.PlaceArrayAdapter;
import com.jalotsav.shsallouvers.common.AppConstants;
import com.jalotsav.shsallouvers.common.GeneralFunctions;
import com.jalotsav.shsallouvers.common.UserSessionManager;
import com.jalotsav.shsallouvers.models.MdlUsers;
import com.jalotsav.shsallouvers.utils.ValidationUtils;

import butterknife.BindString;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by Jalotsav on 5/26/18.
 */

public class ActvtySignUp extends AppCompatActivity implements AppConstants, GoogleApiClient.OnConnectionFailedListener,
        GoogleApiClient.ConnectionCallbacks {

    private static final String TAG = ActvtySignUp.class.getSimpleName();

    @BindView(R.id.cordntrlyot_signup) CoordinatorLayout mCrdntrlyot;

    @BindView(R.id.txtinputlyot_signup_firstname) TextInputLayout mTxtinptlyotFirstName;
    @BindView(R.id.txtinputlyot_signup_lastname) TextInputLayout mTxtinptlyotLastName;
    @BindView(R.id.txtinputlyot_signup_email) TextInputLayout mTxtinptlyotEmail;
    @BindView(R.id.txtinputlyot_signup_companyname) TextInputLayout mTxtinptlyotCompanyName;
    @BindView(R.id.txtinputlyot_signup_city) TextInputLayout mTxtinptlyotCity;
    @BindView(R.id.txtinputlyot_signup_mobileno) TextInputLayout mTxtinptlyotMobile;
    @BindView(R.id.txtinputlyot_signup_password) TextInputLayout mTxtinptlyotPaswrd;

    @BindView(R.id.txtinptet_signup_firstname) TextInputEditText mTxtinptEtFirstName;
    @BindView(R.id.txtinptet_signup_lastname) TextInputEditText mTxtinptEtLastName;
    @BindView(R.id.txtinptet_signup_email) TextInputEditText mTxtinptEtEmail;
    @BindView(R.id.txtinptet_signup_companyname) TextInputEditText mTxtinptEtCompanyName;
    @BindView(R.id.txtinptet_signup_mobileno) TextInputEditText mTxtinptEtMobile;
    @BindView(R.id.txtinptet_signup_password) TextInputEditText mTxtinptEtPaswrd;
    @BindView(R.id.autocmplttv_signup_city) AutoCompleteTextView mAutocmplttvCity;

    @BindView(R.id.prgrsbr_signup) ProgressBar mPrgrsbrMain;

    @BindString(R.string.no_intrnt_cnctn) String mNoInternetConnMsg;
    @BindString(R.string.server_problem_sml) String mServerPrblmMsg;
    @BindString(R.string.internal_problem_sml) String mInternalPrblmMsg;
    @BindString(R.string.entr_firstname_sml) String mEntrFirstName;
    @BindString(R.string.entr_lastname_sml) String mEntrLastName;
    @BindString(R.string.entr_email_sml) String mEntrEmail;
    @BindString(R.string.email_exist) String mEmailExistMsg;
    @BindString(R.string.entr_valid_city_sml) String mEntrSelectValidCity;
    @BindString(R.string.mobileno_exist) String mMobileExistMsg;
    @BindString(R.string.mobileno_verfd_sucsfly) String mMobileVerifedMsg;
    @BindString(R.string.mobileno_not_verfd) String mMobileNotVerifiedMsg;
    @BindString(R.string.exist_sml) String mExistStr;
    @BindString(R.string.sucsfly_regstr_sml) String mSucsflyRegstrnMsg;

    private static final int GOOGLE_API_CLIENT_ID = 0;
    private GoogleApiClient mGoogleApiClient;
    private PlaceArrayAdapter mPlaceArrayAdapter;
    /*private static final LatLngBounds BOUNDS_MOUNTAIN_VIEW = new LatLngBounds(
            new LatLng(37.398160, -122.180831), new LatLng(37.430610, -121.972090));*/
    String mSelectedCityVal, mPhoneAuthUid = "", mFirstNameVal, mLastNameVal, mEmailVal, mCompanyNameVal = "", mMobileVal, mPasswordVal;
    UserSessionManager session;
    boolean isMobileVerified = false;
    FirebaseDatabase mDatabase;
    DatabaseReference mRootRef, mUsersRef;
    private FirebaseAuth mFireAuth;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.lo_actvty_signup);
        ButterKnife.bind(this);

        ActionBar mActionBar = getSupportActionBar();
        if (mActionBar != null)
            mActionBar.setDisplayHomeAsUpEnabled(true);

        session = new UserSessionManager(this);
        mFireAuth = FirebaseAuth.getInstance();
//
        // Firebase Database Initialization and References
        mDatabase = FirebaseDatabase.getInstance();
        mRootRef = mDatabase.getReference().child(ROOT_NAME);
        mUsersRef = mRootRef.child(CHILD_USERS);

        // Google Place API configuration
        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .addApi(Places.GEO_DATA_API)
                .enableAutoManage(this, GOOGLE_API_CLIENT_ID, this)
                .addConnectionCallbacks(this)
                .build();

        mAutocmplttvCity.setThreshold(3);
        mAutocmplttvCity.setOnItemClickListener(mAutocompleteClickListener);
        AutocompleteFilter autocompleteFilter = new AutocompleteFilter.Builder()
                .setTypeFilter(AutocompleteFilter.TYPE_FILTER_CITIES)
                /*.setCountry("IN")*/
                .build();
        mPlaceArrayAdapter = new PlaceArrayAdapter(this, android.R.layout.simple_list_item_1, null, autocompleteFilter);
        mAutocmplttvCity.setAdapter(mPlaceArrayAdapter);
    }

    @OnClick({R.id.appcmptbtn_signup})
    public void onClickView(View view) {

        switch (view.getId()) {
            case R.id.appcmptbtn_signup:

                if(mPrgrsbrMain.getVisibility() != View.VISIBLE) {
                    if (GeneralFunctions.isNetConnected(this))
                        checkAllValidation();
                    else
                        Snackbar.make(mCrdntrlyot, mNoInternetConnMsg, Snackbar.LENGTH_LONG).show();
                }
                break;
        }
    }

    // ItemClickListener for AutoComplete of City
    private AdapterView.OnItemClickListener mAutocompleteClickListener = new AdapterView.OnItemClickListener() {

        @Override
        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

            final PlaceArrayAdapter.PlaceAutocomplete item = mPlaceArrayAdapter.getItem(position);
            final String placeId = String.valueOf(item.placeId);
            Log.i(TAG, "Selected: " + item.description);
            PendingResult<PlaceBuffer> placeResult = Places.GeoDataApi.getPlaceById(mGoogleApiClient, placeId);
            placeResult.setResultCallback(mUpdatePlaceDetailsCallback);
            Log.i(TAG, "Fetching details for ID: " + item.placeId);
        }
    };

    private ResultCallback<PlaceBuffer> mUpdatePlaceDetailsCallback = new ResultCallback<PlaceBuffer>() {

        @Override
        public void onResult(PlaceBuffer places) {

            if (!places.getStatus().isSuccess()) {
                Log.e(TAG, "Place query did not complete. Error: " + places.getStatus().toString());
                return;
            }
            // Selecting the first object buffer.
            final Place place = places.get(0);
            mSelectedCityVal = String.valueOf(Html.fromHtml(place.getAddress() + ""));
        }
    };

    // Check all validation of fields and call API
    private void checkAllValidation() {

        if (!ValidationUtils.validateEmpty(this, mTxtinptlyotFirstName, mTxtinptEtFirstName, mEntrFirstName)) // FirstName
            return;

        if (!ValidationUtils.validateEmpty(this, mTxtinptlyotLastName, mTxtinptEtLastName, mEntrLastName)) // LastName
            return;

        if (!ValidationUtils.validateEmpty(this, mTxtinptlyotEmail, mTxtinptEtEmail, mEntrEmail)) // Email
            return;

        if (!ValidationUtils.validateEmailFormat(this, mTxtinptlyotEmail, mTxtinptEtEmail)) // Email Format
            return;

        if (!ValidationUtils.validateEmpty(mTxtinptlyotCity, mSelectedCityVal, mEntrSelectValidCity)) // City
            return;
        mAutocmplttvCity.setText(mSelectedCityVal); // Set selected value from Place API, in case user changed after selection

        if (!ValidationUtils.validateMobile(this, mTxtinptlyotMobile, mTxtinptEtMobile)) // Mobile
            return;

        if (!ValidationUtils.validatePassword(this, mTxtinptlyotPaswrd, mTxtinptEtPaswrd)) // Password
            return;

        checkEmailExist();
    }

    // Check for Email is already exist in our Firebase Database
    private void checkEmailExist() {

        mPrgrsbrMain.setVisibility(View.VISIBLE);
        mEmailVal = mTxtinptEtEmail.getText().toString().trim();
        // Check Email is Exist in database
        mUsersRef.orderByChild(CHILD_EMAIL).equalTo(mEmailVal)
                .addListenerForSingleValueEvent(new ValueEventListener() {
                    @Override
                    public void onDataChange(@NonNull DataSnapshot dataSnapshot) {

                        mPrgrsbrMain.setVisibility(View.GONE);
                        if(dataSnapshot.exists())
                            Snackbar.make(mCrdntrlyot, mEmailExistMsg, Snackbar.LENGTH_LONG).show();
                        else
                            checkMobileExist();
                    }

                    @Override
                    public void onCancelled(@NonNull DatabaseError databaseError) {

                        Log.e(TAG, "checkEmailExist - onCancelled: " + databaseError.getMessage());
                        mPrgrsbrMain.setVisibility(View.GONE);
                        Snackbar.make(mCrdntrlyot, mServerPrblmMsg, Snackbar.LENGTH_LONG).show();
                    }
                });
    }

    // Check for Mobile number is already exist in our Firebase Database
    private void checkMobileExist() {

        mPrgrsbrMain.setVisibility(View.VISIBLE);
        mMobileVal = mTxtinptEtMobile.getText().toString().trim();
        // Check Mobile number is Exist in database
        mUsersRef.orderByChild(CHILD_MOBILE).equalTo(mMobileVal)
                .addListenerForSingleValueEvent(new ValueEventListener() {
                    @Override
                    public void onDataChange(@NonNull DataSnapshot dataSnapshot) {

                        mPrgrsbrMain.setVisibility(View.GONE);
                        if(dataSnapshot.exists())
                            Snackbar.make(mCrdntrlyot, mMobileExistMsg, Snackbar.LENGTH_LONG).show();
                        else {

                            if(!isMobileVerified) {

                                Intent intntVerifyMobileNo = new Intent(ActvtySignUp.this, VerifyMobileNo.class);
                                intntVerifyMobileNo.putExtra(AppConstants.CHILD_MOBILE, mTxtinptEtMobile.getText().toString().trim());
                                startActivityForResult(intntVerifyMobileNo, AppConstants.REQUEST_VERFCTN_MOBILENO);
                            } else
                                writeNewUser();
                        }
                    }

                    @Override
                    public void onCancelled(@NonNull DatabaseError databaseError) {

                        Log.e(TAG, "checkMobileExist - onCancelled: " + databaseError.getMessage());
                        mPrgrsbrMain.setVisibility(View.GONE);
                        Snackbar.make(mCrdntrlyot, mServerPrblmMsg, Snackbar.LENGTH_LONG).show();
                    }
                });
    }

    // Write/Add new user in Firebase Database
    private void writeNewUser() {

        mFirstNameVal =  mTxtinptEtFirstName.getText().toString().trim();
        mFirstNameVal = Character.toUpperCase(mFirstNameVal.charAt(0))+mFirstNameVal.substring(1); // First Character Uppercase
        mLastNameVal = mTxtinptEtLastName.getText().toString().trim();
        mLastNameVal = Character.toUpperCase(mLastNameVal.charAt(0)) + mLastNameVal.substring(1); // First Character Uppercase
        mEmailVal = mTxtinptEtEmail.getText().toString().trim();
        mCompanyNameVal = mTxtinptEtCompanyName.getText().toString().trim();
        mMobileVal = mTxtinptEtMobile.getText().toString().trim();
        mPasswordVal = mTxtinptEtPaswrd.getText().toString().trim();

        DatabaseReference newUsersRef = mUsersRef.child(mPhoneAuthUid);
        newUsersRef.setValue(
                new MdlUsers(mPhoneAuthUid, mFirstNameVal, mLastNameVal, mEmailVal, mCompanyNameVal,
                        mSelectedCityVal, mMobileVal, mPasswordVal, DEVICE_TYPE_ANDROID, true))
                .addOnSuccessListener(new OnSuccessListener<Void>() {
                    @Override
                    public void onSuccess(Void aVoid) {

                        Log.i(TAG, "onSuccess: writeNewUser - data successfully inserted.");
                        session.setUserId(mPhoneAuthUid);
                        session.setFirstName(mFirstNameVal);
                        session.setLastName(mLastNameVal);
                        session.setEmail(mEmailVal);
                        session.setCompanyName(mCompanyNameVal);
                        session.setCity(mSelectedCityVal);
                        session.setMobile(mMobileVal);

                        Toast.makeText(ActvtySignUp.this, mSucsflyRegstrnMsg, Toast.LENGTH_SHORT).show();
                        finish();
                        startActivity(new Intent(ActvtySignUp.this, ActvtyMain.class));
                    }
                })
                .addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {

                        Log.e(TAG, "onFailure: writeNewUser - " + e.getMessage());
                        Snackbar.make(mCrdntrlyot, mServerPrblmMsg, Snackbar.LENGTH_LONG).show();
                    }
                });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if(requestCode == AppConstants.REQUEST_VERFCTN_MOBILENO) {

            if(resultCode == RESULT_OK) {

                isMobileVerified = true;
                Snackbar.make(mCrdntrlyot, mMobileVerifedMsg, Snackbar.LENGTH_SHORT).show();

                mPhoneAuthUid = data.getStringExtra(AppConstants.CHILD_UID);
                writeNewUser();
            } else {

                isMobileVerified = false;
                mFireAuth.signOut();
                Snackbar.make(mCrdntrlyot, mMobileNotVerifiedMsg, Snackbar.LENGTH_SHORT).show();
            }
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {
            case android.R.id.home:

                onBackPressed();
                break;
            default:
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onConnected(Bundle bundle) {

        mPlaceArrayAdapter.setGoogleApiClient(mGoogleApiClient);
        Log.i(TAG, "Google Places API connected.");
    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

        Log.e(TAG, "Google Places API connection failed with error code: " + connectionResult.getErrorCode());
        Toast.makeText(this, mServerPrblmMsg, Toast.LENGTH_LONG).show();
    }

    @Override
    public void onConnectionSuspended(int i) {
        mPlaceArrayAdapter.setGoogleApiClient(null);
        Log.e(TAG, "Google Places API connection suspended.");
    }
}
