/*
 * Copyright (c) 2018 Jalotsav
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.jalotsav.shsallouvers;

import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.jalotsav.shsallouvers.adapter.ImagesPreviewPageAdapter;
import com.jalotsav.shsallouvers.common.AppConstants;
import com.jalotsav.shsallouvers.models.MdlImages;

import java.util.ArrayList;

import butterknife.BindDrawable;
import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Jalotsav on 5/31/18.
 */

public class DialogFrgmntImagePreview extends DialogFragment implements AppConstants {

    @BindView(R.id.viewpgr_frgmnt_images_preview) ViewPager mViewPager;

    @BindDrawable(R.drawable.ic_window_colorful) Drawable mDrwblDefault;

    ArrayList<MdlImages> mArrylstMdlImages;
    ImagesPreviewPageAdapter mAdapter;
    int mSlctdPosition;

    static DialogFrgmntImagePreview newInstance() {
        return new DialogFrgmntImagePreview();
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        View rootView = inflater.inflate(R.layout.lo_frgmnt_images_preview, container, false);
        ButterKnife.bind(this, rootView);

        mArrylstMdlImages = (ArrayList<MdlImages>) getArguments().getSerializable(PUT_EXTRA_IMAGES_MODEL);
        mSlctdPosition = getArguments().getInt(PUT_EXTRA_POSITION);

        mAdapter = new ImagesPreviewPageAdapter(getActivity(), mArrylstMdlImages, mDrwblDefault);
        mViewPager.setAdapter(mAdapter);

        mViewPager.setCurrentItem(mSlctdPosition, false);

        return rootView;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setStyle(DialogFragment.STYLE_NORMAL, android.R.style.Theme_Black_NoTitleBar_Fullscreen);
    }
}
