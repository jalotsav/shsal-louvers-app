/*
 * Copyright (c) 2018 Jalotsav
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.jalotsav.shsallouvers.adapter;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.support.annotation.NonNull;
import android.support.v4.view.PagerAdapter;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.jalotsav.shsallouvers.R;
import com.jalotsav.shsallouvers.models.MdlImages;

import java.util.ArrayList;

/**
 * Created by Jalotsav on 5/31/18.
 */

public class ImagesPreviewPageAdapter extends PagerAdapter {

    private Context mContext;
    private ArrayList<MdlImages> mArrylstMdlImages;
    private Drawable mDrwblDefault;

    public ImagesPreviewPageAdapter(Context context, ArrayList<MdlImages> arrylstMdlImages, Drawable drwblDefault) {

        this.mContext = context;
        this.mArrylstMdlImages = arrylstMdlImages;
        mDrwblDefault = drwblDefault;
    }

    @Override
    public Object instantiateItem(@NonNull ViewGroup container, int position) {

        LayoutInflater inflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        View itemView = inflater.inflate(R.layout.lo_vwpgr_item_images_preview, container, false);
        ImageView mImgvwImage = itemView.findViewById(R.id.imgvw_vwpgr_item_imageprevw_image);

        String imagePath = mArrylstMdlImages.get(position).getImageURL();
        if (!TextUtils.isEmpty(imagePath)) {
            Glide.with(mContext)
                    .load(imagePath)
                    .apply(new RequestOptions().placeholder(mDrwblDefault))
                    .into(mImgvwImage);
        }
        container.addView(itemView);

        return itemView;
    }

    @Override
    public int getCount() {
        return mArrylstMdlImages.size();
    }

    @Override
    public boolean isViewFromObject(@NonNull View view, @NonNull Object object) {
        return view == object;
    }

    @Override
    public void destroyItem(@NonNull ViewGroup container, int position, @NonNull Object object) {
        container.removeView((View) object);
    }
}
