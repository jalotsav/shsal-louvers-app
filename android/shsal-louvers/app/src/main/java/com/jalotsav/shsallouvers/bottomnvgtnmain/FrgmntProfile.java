/*
 * Copyright (c) 2018 Jalotsav
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.jalotsav.shsallouvers.bottomnvgtnmain;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.CoordinatorLayout;
import android.support.v4.app.Fragment;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.jalotsav.shsallouvers.ActvtyProfileEdit;
import com.jalotsav.shsallouvers.R;
import com.jalotsav.shsallouvers.common.AppConstants;
import com.jalotsav.shsallouvers.common.UserSessionManager;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by Jalotsav on 5/28/18.
 */

public class FrgmntProfile extends Fragment {

    @BindView(R.id.cordntrlyot_frgmnt_profile) CoordinatorLayout mCrdntrlyot;

    @BindView(R.id.tv_frgmnt_profile_firstcharname) TextView mTvFirstCharName;
    @BindView(R.id.tv_frgmnt_profile_fullname) TextView mTvFullName;
    @BindView(R.id.tv_frgmnt_profile_mobile) TextView mTvmobile;
    @BindView(R.id.tv_frgmnt_profile_email) TextView mTvEmail;
    @BindView(R.id.tv_frgmnt_profile_city) TextView mTvCity;
    @BindView(R.id.tv_frgmnt_profile_company) TextView mTvCompany;

    @BindView(R.id.lnrlyot_frgmnt_profile_company) LinearLayout mLnrlyotCompany;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        View rootView = inflater.inflate(R.layout.lo_frgmnt_profile, container, false);
        ButterKnife.bind(this, rootView);

        fetchUserDetails();

        return rootView;
    }

    // Fetch User Profile Details from SharedPreferences
    private void fetchUserDetails() {

        UserSessionManager session = new UserSessionManager(getActivity());
        mTvFirstCharName.setText(session.getFirstName().substring(0,1).toUpperCase());
        mTvFullName.setText(session.getFirstName().concat(" ").concat(session.getLastName()));
        mTvmobile.setText(session.getMobile());
        mTvEmail.setText(session.getEmail());
        mTvCity.setText(session.getCity());
        if(TextUtils.isEmpty(session.getCompanyName()))
            mLnrlyotCompany.setVisibility(View.GONE);
        else {
            mLnrlyotCompany.setVisibility(View.VISIBLE);
            mTvCompany.setText(session.getCompanyName());
        }
    }

    @OnClick({R.id.fab_frgmnt_profile_edit})
    public void onClickView(View view) {

        switch (view.getId()) {
            case R.id.fab_frgmnt_profile_edit:

                startActivityForResult(new Intent(getActivity(), ActvtyProfileEdit.class), AppConstants.REQUEST_PROFILE_EDIT);
                break;
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if(resultCode == Activity.RESULT_OK) {
            if(requestCode == AppConstants.REQUEST_PROFILE_EDIT)
                fetchUserDetails();
        }
    }
}
