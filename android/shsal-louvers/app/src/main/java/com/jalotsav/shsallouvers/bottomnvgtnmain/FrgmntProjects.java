/*
 * Copyright (c) 2018 Jalotsav
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.jalotsav.shsallouvers.bottomnvgtnmain;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.firebase.ui.database.FirebaseRecyclerAdapter;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.jalotsav.shsallouvers.ActvtyMain;
import com.jalotsav.shsallouvers.ActvtyProjectsImages;
import com.jalotsav.shsallouvers.R;
import com.jalotsav.shsallouvers.common.AppConstants;
import com.jalotsav.shsallouvers.common.GeneralFunctions;
import com.jalotsav.shsallouvers.common.RecyclerViewEmptySupport;
import com.jalotsav.shsallouvers.models.MdlProjects;
import com.jalotsav.shsallouvers.viewholders.ProjectsViewHolder;

import butterknife.BindString;
import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Jalotsav on 5/28/18.
 */

public class FrgmntProjects extends Fragment implements AppConstants {

    @BindView(R.id.cordntrlyot_frgmnt_projects) CoordinatorLayout mCrdntrlyot;
    @BindView(R.id.lnrlyot_recyclremptyvw_appearhere) LinearLayout mLnrlyotAppearHere;
    @BindView(R.id.tv_recyclremptyvw_appearhere) TextView mTvAppearHere;
    @BindView(R.id.rcyclrvw_frgmnt_projects) RecyclerViewEmptySupport mRecyclerView;
    @BindView(R.id.prgrsbr_frgmnt_projects) ProgressBar mPrgrsbr;

    @BindString(R.string.projects_appear_here) String mProjectsAppearHere;
    @BindString(R.string.no_intrnt_cnctn) String mNoInternetConnMsg;

    RecyclerView.LayoutManager mLayoutManager;
    FirebaseRecyclerAdapter<MdlProjects, ProjectsViewHolder> mAdapter;
    FirebaseDatabase mDatabase;
    DatabaseReference mRootRef, mProjectsRef;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        View rootView = inflater.inflate(R.layout.lo_frgmnt_projects, container, false);
        ButterKnife.bind(this, rootView);

        setHasOptionsMenu(true);

        mLayoutManager = new LinearLayoutManager(getActivity());
        mRecyclerView.setHasFixedSize(true);
        mRecyclerView.setLayoutManager(mLayoutManager);
        mRecyclerView.setEmptyView(mLnrlyotAppearHere);

        mTvAppearHere.setText(mProjectsAppearHere);

        // Firebase Database Initialization and References
        mDatabase = FirebaseDatabase.getInstance();
        mRootRef = mDatabase.getReference().child(ROOT_NAME);
        mProjectsRef = mRootRef.child(CHILD_PROJECTS);

        if(GeneralFunctions.isNetConnected(getActivity()))
            setupFirebaseAdapter();
        else Toast.makeText(getActivity(), mNoInternetConnMsg, Toast.LENGTH_SHORT).show();

        return rootView;
    }

    // Setup Recycler Adapter using Firebase-UI with Realtime database support.
    private void setupFirebaseAdapter() {

        mPrgrsbr.setVisibility(View.VISIBLE);
        // Query: get only isImagesAvailable = TRUE projects
        Query queryProjectsRef = mProjectsRef.orderByChild(CHILD_IMAGES_AVAILABLE).equalTo(true);
        mAdapter = new FirebaseRecyclerAdapter<MdlProjects, ProjectsViewHolder>(
                MdlProjects.class,
                R.layout.lo_recyclritem_projectslst,
                ProjectsViewHolder.class,
                queryProjectsRef
        ) {
            @Override
            public void onDataChanged() {
                super.onDataChanged();

                if(mPrgrsbr != null && mPrgrsbr.getVisibility() == View.VISIBLE)
                    mPrgrsbr.setVisibility(View.GONE);
                mAdapter.notifyDataSetChanged();
            }

            @Override
            protected void populateViewHolder(ProjectsViewHolder viewHolder, final MdlProjects model, final int position) {

                viewHolder.setProjectName(model.getName().toUpperCase());
                viewHolder.setProjectImage(getActivity(), model.getCoverImageURL());

                viewHolder.itemView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {

                        startActivity(new Intent(getActivity(), ActvtyProjectsImages.class)
                                .putExtra(PUT_EXTRA_DBREF_CHILD_KEY, mAdapter.getRef(position).getKey())
                                .putExtra(PUT_EXTRA_PROJECT_NAME, model.getName()));
                    }
                });
            }
        };
        mRecyclerView.setAdapter(mAdapter);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.menu_refresh, menu);
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {
            case R.id.action_refresh:

                if(GeneralFunctions.isNetConnected(getActivity()))
                    setupFirebaseAdapter();
                else Toast.makeText(getActivity(), mNoInternetConnMsg, Toast.LENGTH_SHORT).show();
                break;
            default:
                break;
        }
        return super.onOptionsItemSelected(item);
    }
}
