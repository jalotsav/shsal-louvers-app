/*
 * Copyright (c) 2018 Jalotsav
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.jalotsav.shsallouvers.bottomnvgtnmain;

import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.Snackbar;
import android.support.design.widget.TextInputEditText;
import android.support.design.widget.TextInputLayout;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.Spinner;

import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.jalotsav.shsallouvers.R;
import com.jalotsav.shsallouvers.common.AppConstants;
import com.jalotsav.shsallouvers.common.GeneralFunctions;
import com.jalotsav.shsallouvers.common.UserSessionManager;
import com.jalotsav.shsallouvers.models.MdlQuotation;
import com.jalotsav.shsallouvers.utils.ValidationUtils;

import butterknife.BindString;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by Jalotsav on 6/13/18.
 */

public class FrgmntQuotation extends Fragment implements AppConstants {

    private static final String TAG = "FrgmntQuotation";

    @BindView(R.id.cordntrlyot_frgmnt_quotation) CoordinatorLayout mCrdntrlyot;
    @BindView(R.id.spnr_frgmnt_quotation_colors) Spinner mSpnrColor;

    @BindView(R.id.txtinputlyot_frgmnt_quotation_width) TextInputLayout mTxtinptlyotWidth;
    @BindView(R.id.txtinputlyot_frgmnt_quotation_height) TextInputLayout mTxtinptlyotHeight;
    @BindView(R.id.txtinputlyot_frgmnt_quotation_quantity) TextInputLayout mTxtinptlyotQty;

    @BindView(R.id.txtinptet_frgmnt_quotation_width) TextInputEditText mTxtinptEtWidth;
    @BindView(R.id.txtinptet_frgmnt_quotation_height) TextInputEditText mTxtinptEtHeight;
    @BindView(R.id.txtinptet_frgmnt_quotation_quantity) TextInputEditText mTxtinptEtQty;

    @BindView(R.id.prgrsbr_frgmnt_quotation) ProgressBar mPrgrsbrMain;

    @BindString(R.string.no_intrnt_cnctn) String mNoInternetConnMsg;
    @BindString(R.string.server_problem_sml) String mServerPrblmMsg;
    @BindString(R.string.internal_problem_sml) String mInternalPrblmMsg;
    @BindString(R.string.entr_width_in_mm_sml) String mEntrWidth;
    @BindString(R.string.entr_height_in_mm_sml) String mEntrHeight;
    @BindString(R.string.value_greater_600_msg) String mValueGreater600Msg;
    @BindString(R.string.entr_quantity_sml) String mEntrQty;
    @BindString(R.string.request_quotation_sml) String mStrRequestQuotation;
    @BindString(R.string.sucsfly_quotation_sml) String mRegstrdQuotationMsg;

    UserSessionManager session;
    FirebaseDatabase mDatabase;
    DatabaseReference mRootRef, mQuotationRef;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        View rootView = inflater.inflate(R.layout.lo_frgmnt_quotation, container, false);
        ButterKnife.bind(this, rootView);

        session = new UserSessionManager(getActivity());

        // Firebase Database Initialization and References
        mDatabase = FirebaseDatabase.getInstance();
        mRootRef = mDatabase.getReference().child(ROOT_NAME);
        mQuotationRef = mRootRef.child(CHILD_QUOTATIONS);

        return rootView;
    }

    @OnClick({R.id.appcmptbtn_frgmnt_quotation_request})
    public void onClickView(View view) {

        switch (view.getId()) {
            case R.id.appcmptbtn_frgmnt_quotation_request:

                if(mPrgrsbrMain.getVisibility() != View.VISIBLE) {
                    if (GeneralFunctions.isNetConnected(getActivity()))
                        checkAllValidation();
                    else
                        Snackbar.make(mCrdntrlyot, mNoInternetConnMsg, Snackbar.LENGTH_LONG).show();
                }
                break;
        }
    }

    // Check all validation of fields and call API
    private void checkAllValidation() {

        if (!ValidationUtils.validateEmpty(getActivity(), mTxtinptlyotWidth, mTxtinptEtWidth, mEntrWidth)) // Width
            return;

        if(!ValidationUtils.validateWidthHeightMinVal(getActivity(), mTxtinptlyotWidth, mTxtinptEtWidth, mValueGreater600Msg)) // Width > 600
            return;

        if (!ValidationUtils.validateEmpty(getActivity(), mTxtinptlyotHeight, mTxtinptEtHeight, mEntrHeight)) // Height
            return;

        if(!ValidationUtils.validateWidthHeightMinVal(getActivity(), mTxtinptlyotHeight, mTxtinptEtHeight, mValueGreater600Msg)) // Height > 600
            return;

        if (!ValidationUtils.validateEmpty(getActivity(), mTxtinptlyotQty, mTxtinptEtQty, mEntrQty)) // Quantity
            return;

        if(!ValidationUtils.validateQtyMinVal(getActivity(), mTxtinptlyotQty, mTxtinptEtQty, mEntrQty)) // Quantity > 0
            return;

        writeNewQuotation();
    }

    // Write/Add new quotation request of user in Firebase Database
    private void writeNewQuotation() {

        mPrgrsbrMain.setVisibility(View.VISIBLE);
        String selectedColor = mSpnrColor.getSelectedItem().toString();
        int widthVal = Integer.parseInt(mTxtinptEtWidth.getText().toString().trim());
        int heightVal = Integer.parseInt(mTxtinptEtHeight.getText().toString().trim());
        int quantityVal = Integer.parseInt(mTxtinptEtQty.getText().toString().trim());

        DatabaseReference newQuotationRef = mQuotationRef.push();
        MdlQuotation objMdlQuotation = new MdlQuotation(
                newQuotationRef.getKey(),
                session.getUserId(), session.getMobile(),
                selectedColor, QUOTATION_STATUS_PENDING, widthVal, heightVal, quantityVal);
        newQuotationRef.setValue(objMdlQuotation)
                .addOnSuccessListener(new OnSuccessListener<Void>() {
                    @Override
                    public void onSuccess(Void aVoid) {

                        Log.i(TAG, "onSuccess: writeNewQuotation - data successfully inserted.");
                        mPrgrsbrMain.setVisibility(View.GONE);
                        clearUI();
                        quotationRegisteredAlertDialog();
                    }

                    // Clear UI - user inserted data
                    private void clearUI() {

                        mSpnrColor.setSelection(0);
                        mTxtinptEtWidth.setText("");
                        mTxtinptEtHeight.setText("");
                        mTxtinptEtQty.setText("");
                    }
                })
                .addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {

                        Log.e(TAG, "onFailure: writeNewQuotation - " + e.getMessage());
                        mPrgrsbrMain.setVisibility(View.GONE);
                        Snackbar.make(mCrdntrlyot, mServerPrblmMsg, Snackbar.LENGTH_LONG).show();
                    }
                });
    }

    // Show AlertDialog for notify - "Quotation request registered"
    private void quotationRegisteredAlertDialog() {

        AlertDialog.Builder alrtDlg = new AlertDialog.Builder(getActivity());
        alrtDlg.setTitle(mStrRequestQuotation);
        alrtDlg.setMessage(mRegstrdQuotationMsg);
        alrtDlg.setPositiveButton(getString(android.R.string.ok).toUpperCase(), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {

                dialogInterface.dismiss();
            }
        });

        alrtDlg.show();
    }
}
