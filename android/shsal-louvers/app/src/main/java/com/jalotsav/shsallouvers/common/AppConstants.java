/*
 * Copyright (c) 2018 Jalotsav
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.jalotsav.shsallouvers.common;

/**
 * Created by Jalotsav on 5/26/18.
 */

public interface AppConstants {

    // Build Type
    String DEBUG_BUILD_TYPE = "debug";

    // FireDB Object
    String ROOT_NAME = "shsallouversApp";
    String CHILD_USERS = "users";
    String CHILD_UID = "uid";
    String CHILD_FIRST_NAME = "firstname";
    String CHILD_LAST_NAME = "lastname";
    String CHILD_EMAIL = "email";
    String CHILD_COMPANY_NAME = "companyname";
    String CHILD_CITY = "city";
    String CHILD_MOBILE = "mobile";
    String CHILD_PROJECTS = "projects";
    String CHILD_IMAGES = "images";
    String CHILD_IMAGES_AVAILABLE = "imagesAvailable";
    String CHILD_VIDEOS_AVAILABLE = "videosAvailable";
    String CHILD_VIDEOS = "videos";
    String CHILD_QUOTATIONS = "quotations";

    // PutExtra Keys
    String PUT_EXTRA_COME_FROM = "comeFrom";
    String PUT_EXTRA_NVGTNVW_POSTN = "bottomNvgtnPosition";
    String PUT_EXTRA_DBREF_CHILD_KEY = "databaseRefChildKey";
    String PUT_EXTRA_PROJECT_NAME = "projectName";
    String PUT_EXTRA_IMAGES_MODEL = "imagesModel";
    String PUT_EXTRA_POSITION = "position";

    // Navigation Drawer MenuItem position check for direct open that fragment
    int NVGTNVW_PROJECTS = 21;
    int NVGTNVW_VIDEOS = 22;
    int NVGTNVW_QUOTATION = 23;
    int NVGTNVW_ABOUTUS = 24;
    int NVGTNVW_CONTACTUS = 25;
    int NVGTNVW_PROFIE = 26;

    // Request Keys
    int REQUEST_VERFCTN_MOBILENO = 101;
    int REQUEST_APP_PERMISSION = 102;
    int REQUEST_PROFILE_EDIT = 103;

    // Quotation Status
    String QUOTATION_STATUS_PENDING = "pending";
    String QUOTATION_STATUS_APPROVED = "approved";
    String QUOTATION_STATUS_REJECTED = "rejected";

    // Others
    String DEVICE_TYPE_ANDROID = "Android";
}
