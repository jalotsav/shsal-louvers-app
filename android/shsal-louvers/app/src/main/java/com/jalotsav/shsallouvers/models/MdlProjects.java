/*
 * Copyright (c) 2018 Jalotsav
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.jalotsav.shsallouvers.models;

import com.google.firebase.database.IgnoreExtraProperties;

/**
 * Created by Jalotsav on 5/29/18.
 */

@IgnoreExtraProperties
public class MdlProjects {

    private String id, name, coverImageURL;
    private boolean imagesAvailable, videosAvailable;

    public MdlProjects() {
    }

    public MdlProjects(String id, String name, String coverImageURL, boolean imagesAvailable, boolean videosAvailable) {
        this.id = id;
        this.name = name;
        this.coverImageURL = coverImageURL;
        this.imagesAvailable = imagesAvailable;
        this.videosAvailable = videosAvailable;
    }

    public String getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getCoverImageURL() {
        return coverImageURL;
    }

    public boolean isImagesAvailable() {
        return imagesAvailable;
    }

    public boolean isVideosAvailable() {
        return videosAvailable;
    }
}
