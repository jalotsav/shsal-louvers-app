/*
 * Copyright (c) 2018 Jalotsav
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.jalotsav.shsallouvers.models;

/**
 * Created by Jalotsav on 6/14/18.
 */

public class MdlQuotation {

    private String id, uId, userMobile, color, status;
    private int width, height, quantity;

    public MdlQuotation() {
    }

    public MdlQuotation(String id, String uId, String userMobile, String color, String status, int width, int height, int quantity) {
        this.id = id;
        this.uId = uId;
        this.userMobile = userMobile;
        this.color = color;
        this.status = status;
        this.width = width;
        this.height = height;
        this.quantity = quantity;
    }

    public String getId() {
        return id;
    }

    public String getuId() {
        return uId;
    }

    public String getUserMobile() {
        return userMobile;
    }

    public String getColor() {
        return color;
    }

    public String getStatus() {
        return status;
    }

    public int getWidth() {
        return width;
    }

    public int getHeight() {
        return height;
    }

    public int getQuantity() {
        return quantity;
    }
}
