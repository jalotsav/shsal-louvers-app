/*
 * Copyright (c) 2018 Jalotsav
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.jalotsav.shsallouvers.models;

import com.google.firebase.database.Exclude;
import com.google.firebase.database.IgnoreExtraProperties;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by Jalotsav on 5/26/18.
 */

@IgnoreExtraProperties
public class MdlUsers {

    private String uId, firstname, lastname, email, companyname, city, mobile, passphrase, deviceType;
    private boolean active;

    public MdlUsers() {
    }

    public MdlUsers(String uId, String firstname, String lastname, String email, String companyname, String city, String mobile, String passphrase, String deviceType, boolean active) {
        this.uId = uId;
        this.firstname = firstname;
        this.lastname = lastname;
        this.email = email;
        this.companyname = companyname;
        this.city = city;
        this.mobile = mobile;
        this.passphrase = passphrase;
        this.deviceType = deviceType;
        this.active = active;
    }

    public MdlUsers(String firstname, String lastname, String email, String companyname, String city) {
        this.firstname = firstname;
        this.lastname = lastname;
        this.email = email;
        this.companyname = companyname;
        this.city = city;
    }

    @Exclude
    public Map<String, Object> toMap() {
        HashMap<String, Object> result = new HashMap<>();
        result.put("firstname", firstname);
        result.put("lastname", lastname);
        result.put("email", email);
        result.put("companyname", companyname);
        result.put("city", city);

        return result;
    }

    public String getuId() {
        return uId;
    }

    public String getFirstname() {
        return firstname;
    }

    public String getLastname() {
        return lastname;
    }

    public String getEmail() {
        return email;
    }

    public String getCompanyname() {
        return companyname;
    }

    public String getCity() {
        return city;
    }

    public String getMobile() {
        return mobile;
    }

    public String getPassphrase() {
        return passphrase;
    }

    public String getDeviceType() {
        return deviceType;
    }

    public boolean isActive() {
        return active;
    }
}
