/*
 * Copyright (c) 2018 Jalotsav
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.jalotsav.shsallouvers.viewholders;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.jalotsav.shsallouvers.R;

/**
 * Created by Jalotsav on 5/30/18.
 */

public class ImagesViewHolder extends RecyclerView.ViewHolder {

    private View mItemView;
    private ImageView mImgvwImage;

    public ImagesViewHolder(View itemView) {
        super(itemView);
        this.mItemView = itemView;
        mImgvwImage = itemView.findViewById(R.id.imgvw_recylrvw_item_images_image);
    }

    public void setImage(Context context, String imageURL) {

        Glide.with(context)
                .load(imageURL)
//                .apply(new RequestOptions().placeholder(R.drawable.ic_window_colorful))
                .into(mImgvwImage);
    }
}
